//
//  main.cpp
//  funC++
//
//  Created by Alexandru Popescu on 11/09/15.
//  Copyright (c) 2015 Alexandru Florinel Popescu. All rights reserved.
//

#include <funcpp_main.h>

int main(int argc, char **argv)
{
    return funcpp::main(argc, argv);
}
