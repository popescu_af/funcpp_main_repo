//
//  funcpp_test.m
//  funcpp_test
//
//  Created by Alexandru Popescu on 01/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#import <XCTest/XCTest.h>

#include <funcpp_test.h>

@interface funcpp_test_class : XCTestCase

@end

@implementation funcpp_test_class

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)test_add_descriptor
{
    XCTAssertTrue(funcpp_test::test_add_descriptor());
}

- (void)test_add_existing_descriptor
{
    XCTAssertTrue(funcpp_test::test_add_existing_descriptor());
}

- (void)test_add_new_descriptor
{
    XCTAssertTrue(funcpp_test::test_add_new_descriptor());
}

- (void)test_add_new_existing_descriptor
{
    XCTAssertTrue(funcpp_test::test_add_new_existing_descriptor());
}

- (void)test_project_creation
{
    XCTAssertTrue(funcpp_test::test_project_creation());
}

- (void)test_project_reading
{
    XCTAssertTrue(funcpp_test::test_project_reading());
}

- (void)test_simple_bundle
{
    XCTAssertTrue(funcpp_test::test_simple_bundle());
}

- (void)test_simple_component
{
    XCTAssertTrue(funcpp_test::test_simple_component());
}

@end
