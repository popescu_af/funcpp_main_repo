//
//  funcpp_main.h
//  funC++
//
//  Created by Alexandru Popescu on 30/09/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef funcpp_main_h
#define funcpp_main_h

namespace funcpp
{
    int main(int argc, char **argv);
}

#endif /* funcpp_main_h */
