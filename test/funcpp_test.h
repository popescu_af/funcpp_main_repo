//
//  funcpp_test.h
//  funC++
//
//  Created by Alexandru Popescu on 01/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef funcpp_test_h
#define funcpp_test_h

#include <project_handler.h>
#include <definitions.h>
#include <string>
#include <translator.h>

#define STRINGIZE_(x) #x
#define STRINGIZE(x) STRINGIZE_(x)

namespace funcpp_test
{
    using namespace funcpp;
    
    inline
    const std::string& unit_test_res_path()
    {
        static const std::string path = STRINGIZE(UNIT_TEST_RES_PATH);
        return path;
    }
    
    inline
    bool test_project_creation()
    {
        const auto project_path = unit_test_res_path() + "test_project_creation";
        const bool result = ProjectHandler::create("test_create", project_path, "lib");
        
        remove_file_dir(project_path);
        create_dir(project_path);
        return result;
    }
    
    inline
    bool test_project_reading()
    {
        const auto project_path = unit_test_res_path() + "test_project_reading";
        ProjectPtr prj = ProjectHandler::open(project_path);
        
        static const string_vector reference = { "FILE0", "FILE1", "FILE2" };
        return (prj && prj->m_descriptors == reference);
    }
    
    inline
    bool test_simple_bundle()
    {
        const auto descriptor_path = unit_test_res_path() + "test_simple_bundle/simple_bundle" + funcpp::k_desc_file_ext;
        Translator t;
        const bool result = t(descriptor_path);
        return result;
    }
    
    inline
    bool test_simple_component()
    {
        const auto descriptor_path = unit_test_res_path() + "test_simple_component/simple_component" + funcpp::k_desc_file_ext;
        Translator t;
        const bool result = t(descriptor_path);
        return result;
    }
    
    inline
    bool test_add_descriptor()
    {
        const auto descriptor_path = unit_test_res_path() + "test_add_desc/dir_desc/test.fco";
        const auto project_path    = unit_test_res_path() + "test_add_desc/dir_proj";
        ProjectPtr prj = ProjectHandler::open(project_path);
        
        const bool result = ( prj && prj->add(descriptor_path) );
        prj->m_descriptors.clear();
        remove_file_dir(project_path + "/desc/test.fco");
        return result;
    }
    
    inline
    bool test_add_existing_descriptor()
    {
        const auto descriptor_path = unit_test_res_path() + "test_add_existing_desc/dir_desc/test.fco";
        const auto project_path    = unit_test_res_path() + "test_add_existing_desc/dir_proj";
        ProjectPtr prj = ProjectHandler::open(project_path);
        
        return ( prj && !prj->add(descriptor_path) );
    }
    
    inline
    bool test_add_new_descriptor()
    {
        const auto project_path = unit_test_res_path() + "test_add_new_desc";
        ProjectPtr prj = ProjectHandler::open(project_path);
        
        const bool result = ( prj && prj->add_new("new_desc") );
        prj->m_descriptors.clear();
        remove_file_dir(project_path + "/desc/new_desc.fco");
        return result;
    }
    
    inline
    bool test_add_new_existing_descriptor()
    {
        const auto project_path = unit_test_res_path() + "test_add_new_existing_desc";
        ProjectPtr prj = ProjectHandler::open(project_path);
        
        return ( prj && !prj->add_new("new_desc") );
    }
}

#endif /* funcpp_test_h */
