//
//  ast_use_case_traverser.h
//  funC++
//
//  Created by Alexandru Popescu on 12/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef ast_use_case_traverser_h
#define ast_use_case_traverser_h

#include <algorithm>
#include <ast_type_visitor.h>
#include <boost/variant/static_visitor.hpp>
#include <sstream>
#include <symbol_table.h>

namespace funcpp
{
    struct UseCaseTraverser : boost::static_visitor<>
    {
        UseCaseTraverser(const std::string& component_name, DependencyList& dependency_collector)
        : m_component_name(component_name)
        , m_type_visitor(dependency_collector)
        {
        }
        
        void operator() (const UseCaseDeclarations& use_cases);
        void operator() (const UseCaseDeclaration&  use_case, const bool clear_stream = true);
        
    private:
        template <typename Visitor>
        friend class boost::detail::variant::invoke_visitor;
        
        void operator() (const Parameters& params);
        void operator() (const Parameter& param);
        
        void operator() (const NothingType& type);
        void operator() (const TypeGeneric& type);
        
        void clear()
        {
            m_ss_header.str(std::string()); // clear contents
            m_ss_cpp.str(std::string());
        }
        
    public:
        std::stringstream m_ss_header; // Stream for header file
        std::stringstream m_ss_cpp;    // Stream for source file
        std::stringstream m_ss_params; // Stream for parameter list
        std::stringstream m_ss_type;   // Stream for return type
        
        std::string m_component_name;
        TypeVisitor m_type_visitor;
        
        bool m_all_ok;
        bool m_const_params; // put 'const' or not before parameter
        bool m_reserved[6];
    };
    
    inline
    void UseCaseTraverser::operator() (const UseCaseDeclarations& use_cases)
    {
        m_all_ok = true;
        clear();
        
        for (const auto& use_case : use_cases)
        {
            m_ss_header << "\n\t";
            m_ss_cpp << "\n";
            (*this)(use_case, false);
            if (!m_all_ok)
                return;
        }
    }
    
    inline
    void UseCaseTraverser::operator() (const UseCaseDeclaration& use_case, const bool clear_stream /* = true*/)
    {
        m_all_ok = true;
        m_ss_type.str(std::string());
        
        if (clear_stream)
            clear();
        
        boost::apply_visitor(*this, use_case.return_type);
        const auto return_type = m_ss_type.str();
        m_ss_header << return_type << " " << use_case.name << "(";
        m_ss_cpp    << return_type << " " << m_component_name << "::" << use_case.name << "(";
        
        // const parameters
        m_const_params = true;
        boost::apply_visitor(*this, use_case.read_parameters);
        const auto read_parameters = m_ss_params.str();
        m_ss_params.str(std::string());
        
        // mutable parameters
        m_const_params = false;
        boost::apply_visitor(*this, use_case.modify_parameters);
        const auto modify_parameters = m_ss_params.str();
        
        std::string separator = "";
        if (read_parameters != "" && modify_parameters != "")
            separator = ", ";
        
        const auto all_parameters = read_parameters + separator + modify_parameters;
        m_ss_header << all_parameters << ");";
        m_ss_cpp    << all_parameters << ")\n{\n";
        
        if (return_type != "void")
        {
            m_ss_cpp << "\t" << return_type << " result;\n";
            m_ss_cpp << "\t" << "// TODO: implement\n";
            m_ss_cpp << "\t" << "return result;\n";
        }
        
        m_ss_cpp << "}\n";
    }
    
    inline
    void UseCaseTraverser::operator() (const Parameters& params)
    {
        if (params.empty())
            return;
        
        m_ss_params.str(std::string());
        
        (*this)(params.front());
        if (!m_all_ok)
            return;
        
        for (auto it = params.begin() + 1, end = params.end(); it != end; ++it)
        {
            m_ss_params << ", ";
            (*this)(*it);
            if (!m_all_ok)
                return;
        }
    }
    
    inline
    void UseCaseTraverser::operator() (const Parameter& param)
    {
        if (m_const_params)
            m_ss_params << "const" << " ";
        
        (*this)(param.type);
        m_ss_params << m_ss_type.str() << " " << param.name;
    }
    
    inline
    void UseCaseTraverser::operator() (const NothingType&)
    {
        m_ss_type.str(std::string());
        m_ss_type << "void";
    }
    
    inline
    void UseCaseTraverser::operator() (const TypeGeneric& type)
    {
        boost::apply_visitor(m_type_visitor, type);
        if (!m_type_visitor.m_all_ok)
        {
            m_all_ok = false;
            return;
        }
        m_ss_type.str(std::string());
        m_ss_type << m_type_visitor.m_ss_decl.str();
    }
}

#endif /* ast_use_case_traverser_h */
