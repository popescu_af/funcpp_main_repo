//
//  definitions.h
//  funC++
//
//  Created by Alexandru Popescu on 01/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef definitions_h
#define definitions_h

#include <fs_utils.h>
#include <string>
#include <vector>

namespace funcpp
{
    const size_t k_num_threads = 8;
}

namespace funcpp
{
    typedef std::vector<std::string> string_vector;
    
    const std::string k_usage_text =
        "usage: funcpp <command> [<args>]\n\n"
        "funcpp commands:\n"
        "(type 'funcpp help <command>' for syntax)\n\n"
        "   create    " "Creates and initializes a funcpp project\n"
        "   add       " "Adds an existent descriptor to a funcpp project\n"
        "   new       " "Creates a new descriptor and adds it to a funcpp project\n"
        "   c++       " "Generates the C++ project for a funcpp project\n"
        "\n";
    
    const std::string k_help_cmd_text =
        "usage: funcpp help <command_name>\n"
        "\n";
    
    const std::string k_create_cmd_text =
        "usage: funcpp create <name> [template=lib] [path=.]\n"
        "\n";
    
    const std::string k_add_cmd_text =
        "usage: funcpp add <desc_path> [path=.]\n"
        "\n";
    
    const std::string k_new_cmd_text =
        "usage: funcpp new <name> [path=.]\n"
        "\n";
    
    const std::string k_cpp_cmd_text =
        "usage: funcpp c++ [path=.]\n"
        "\n";
    
    const string_vector k_known_funcpp_tags = { "PRODUCT:", "TYPE:", "EXPORT:", "DESCRIPTOR:" };
    const string_vector k_known_funcpp_prj_types = { "lib", "app" };
    const std::string   k_project_file_name = "funcpp";
    const std::string   k_desc_file_ext = ".fco";
    const std::string   k_header_delimiter = "<<HEADER_DELIM>>";
    const std::string   k_pointer_type = "std::unique_ptr";
}

namespace funcpp
{
    bool path_has_funcpp(const std::string& path);
    bool path_is_descriptor(const std::string& path);
    
    inline
    bool path_has_funcpp(const std::string& path)
    {
        return (file_dir_exists(path) &&
                path.rfind(k_project_file_name) != std::string::npos &&
                path.substr( path.rfind(k_project_file_name) ) == k_project_file_name);
    }
    
    inline
    bool path_is_descriptor(const std::string& path)
    {
        return (file_dir_exists(path) &&
                path.rfind(k_desc_file_ext) != std::string::npos &&
                path.substr( path.rfind(k_desc_file_ext) ) == k_desc_file_ext);
    }
}

#endif /* definitions_h */
