//
//  ast_generator_wrapper.cpp
//  funC++
//
//  Created by Alexandru Popescu on 19/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#include <ast_generator.h>
#include <ast_generator_wrapper.h>
#include <iostream>

namespace funcpp
{
    typedef Grammar<std::string::const_iterator> FuncppGrammar;
    
    bool generate_ast_static(const std::string& source_file_text, SourceFileAst& ast)
    {
        static FuncppGrammar grammar; // Unique instance of the grammar.
        std::cout << "parsing=";
        
        using boost::spirit::ascii::space;
        std::string::const_iterator iter = source_file_text.begin();
        std::string::const_iterator end = source_file_text.end();
        bool parsing_result = phrase_parse(iter, end, grammar, space, ast);
        
        if (parsing_result && iter == end)
        {
            std::cout << "OK" "\n";
            return true;
        }
        
        std::string::const_iterator some = iter + 32;
        std::string context(iter, (some > end) ? end : some);
        std::cout << "failed\n";
        std::cerr << "-------------------------\n";
        std::cerr << "Stopped at: \n\"" << context << "...\"\n";
        std::cerr << "-------------------------\n";
        return false;
    }
}
