//
//  ast_generator_wrapper.h
//  funC++
//
//  Created by Alexandru Popescu on 19/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef ast_generator_wrapper_h
#define ast_generator_wrapper_h

#include <ast_structures.h>
#include <string>

namespace funcpp
{
    bool generate_ast_static(const std::string& source_file_text, SourceFileAst& ast);
}

#endif /* ast_generator_wrapper_h */
