//
//  commons.h
//  funC++
//
//  Created by Alexandru Popescu on 26/11/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef commons_h
#define commons_h

#include <csignal>

#ifdef DEBUG
#define BREAKPOINT std::raise(SIGINT)
#else
#define BREAKPOINT /* nada */
#endif

#define UNUSED(x) static_cast<void>(x)

#endif /* commons_h */
