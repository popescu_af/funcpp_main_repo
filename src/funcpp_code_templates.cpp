//
//  funcpp_code_templates.cpp
//  funC++
//
//  Created by Alexandru Popescu on 03/12/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#include "funcpp_code_templates.h"
#include <sstream>

namespace funcpp
{
    std::string create_funcpp_main_component(const std::string& component_name)
    {
        std::stringstream str;
        
        str << "component " << component_name << "\n"
               "{"             "\n"
               "sub:"          "\n"
                               "\n"
               "knowledge:"    "\n"
                               "\n"
               "interface:"    "\n"
                               "\n"
               "};"            "\n";
        
        return str.str();
    }
    
    std::string create_cpp_class(const std::string& class_name, const uint32_t indent_level,
                                 const string_vector& apis, const string_vector& sub_components, const string_vector& attributes)
    {
        const std::string indent = std::string(indent_level, '\n');
        
        std::stringstream str;
        
        str << indent << "class " << class_name << "\n"
            << indent << "{" "\n"
            << indent << "public: // Interface" "\n";
        
        for (const auto& api : apis)
            str << indent << "\t" << api << "\n";

        str << indent << "\n"
            << indent << "private: // Subcomponents" "\n";
        
        for (const auto& sub : sub_components)
            str << indent << "\t" << sub << "\n";

        str << indent << "\n"
            << indent << "private: // Knowledge" "\n";
        
        for (const auto& attr : attributes)
            str << indent << "\t" << attr << "\n";
        
        str << indent << "\n"
            << indent << "};" "\n";
        
        return str.str();
    }
    
    std::string create_cmakelists_header(const std::string& project_name, const uint32_t project_major_version, const uint32_t project_minor_version)
    {
        std::stringstream str;
        
        str << "# Header #" "\n"
            << "cmake_minimum_required(VERSION 3.0 FATAL_ERROR)" << "\n"
            << "project(" << project_name << " VERSION "
            << std::to_string(project_major_version) << "." << std::to_string(project_minor_version)
            << " LANGUAGES CXX)" << "\n";
        
        return str.str();
    }
    
    std::string create_cmakelists_lib_target(const std::string& target_name, const string_vector& sources)
    {
        std::stringstream str;
        
        str << "# Target #" "\n"
        << "add_library(" << target_name << " STATIC" "\n";
        
        for (const auto& src : sources)
            str << "\t" << src << "\n";
        
        str << ")" "\n";
        
        return str.str();
    }
    
    std::string create_cmakelists_app_target(const std::string& target_name, const string_vector& sources)
    {
        std::stringstream str;
        
        str << "# Target #" "\n"
        << "add_executable(" << target_name << "\n";
        
        for (const auto& src : sources)
            str << "\t" << src << "\n";
        
        str << ")" "\n";
        
        return str.str();
    }

}
