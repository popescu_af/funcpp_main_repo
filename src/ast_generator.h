//
//  ast_generator.h
//  funC++
//
//  Created by Alexandru Popescu on 23/09/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef ast_generator_h
#define ast_generator_h

#include <ast_structures.h>

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/foreach.hpp>

namespace funcpp
{
    namespace fusion = boost::fusion;
    namespace phoenix = boost::phoenix;
    namespace qi = boost::spirit::qi;
    namespace ascii = boost::spirit::ascii;
    
    template <typename Iterator>
    struct Grammar : qi::grammar<Iterator, SourceFileAst(), ascii::space_type>
    {
        Grammar();
        
    private: // Rules
        qi::rule<Iterator, std::string(), ascii::space_type> identifier_rule;
        qi::rule<Iterator, NothingType(), ascii::space_type> nothing_rule;
        qi::rule<Iterator, BundleType(), ascii::space_type> bundle_type_rule;
        qi::rule<Iterator, IntegerType(), ascii::space_type> integer_type_rule;
        qi::rule<Iterator, RealType(), ascii::space_type> real_type_rule;
        qi::rule<Iterator, HighPrecisionType(), ascii::space_type> high_precision_type_rule;
        qi::rule<Iterator, TypeGeneric(), ascii::space_type> type_rule;
        qi::rule<Iterator, VariableDeclaration(), ascii::space_type> variable_declaration_rule;
        qi::rule<Iterator, BundleDeclaration(), ascii::space_type> bundle_declaration_rule;
        qi::rule<Iterator, SubDeclaration(), ascii::space_type> sub_declaration_rule;
        qi::rule<Iterator, Parameter(), ascii::space_type> parameter_declaration_rule;
        qi::rule<Iterator, Parameters(), ascii::space_type> parameter_list_declaration_rule;
        qi::rule<Iterator, AgnosticParameters(), ascii::space_type> agnostic_parameters_declaration_rule;
        qi::rule<Iterator, AgnosticType(), ascii::space_type> agnostic_type_rule;
        qi::rule<Iterator, UseCaseDeclaration(), ascii::space_type> use_case_declaration_rule;
        qi::rule<Iterator, ComponentDeclaration(), ascii::space_type> component_declaration_rule;
        qi::rule<Iterator, std::string(), ascii::space_type> module_name_rule;
        qi::rule<Iterator, SourceFileAst(), ascii::space_type> start_rule;
    };
    
    template <typename Iterator>
    Grammar<Iterator>::Grammar() : Grammar<Iterator>::base_type(start_rule)
    {
        using ascii::alnum;
        using ascii::alpha;
        using ascii::char_;
        using qi::double_;
        using qi::float_;
        using qi::int_;
        using qi::lexeme;
        using qi::lit;
        using namespace qi::labels;
        
        using ascii::string;
        using phoenix::at_c;
        using phoenix::push_back;
        
        /* RULES */
        
        // identifier: type or name
        identifier_rule = ( ( alpha | char_('_') ) >> *( alnum | char_('_') ) ) ;
        
        // nothing
        nothing_rule = lit("nothing")[_val = NothingType()];
        
        // bundle<type_name>
        bundle_type_rule = ( lit("bundle") >> char_('<') >> identifier_rule[at_c<0>(_val) = _1] >> char_('>') ) ;
        
        // numeric<integer, [min, max]>
        integer_type_rule = lit("number") >> char_('<') >> lit("integer") >> char_(',')
        >> ( char_('(') | char_('[') )  [at_c<3>(_val) = _1]
        >> int_                         [at_c<0>(_val) = _1]
        >> char_(',')
        >> int_                         [at_c<1>(_val) = _1]
        >> ( char_(')') | char_(']') )  [at_c<4>(_val) = _1]
        >> char_(',')
        >> int_                         [at_c<2>(_val) = _1]
        >> char_('>') ;
        
        // numeric<real, [min, max]>
        real_type_rule = lit("number") >> char_('<') >> lit("real") >> char_(',')
        >> ( char_('(') | char_('[') )  [at_c<3>(_val) = _1]
        >> float_                       [at_c<0>(_val) = _1]
        >> char_(',')
        >> float_                       [at_c<1>(_val) = _1]
        >> ( char_(')') | char_(']') )  [at_c<4>(_val) = _1]
        >> char_(',')
        >> float_                       [at_c<2>(_val) = _1]
        >> char_('>') ;
        
        // numeric<high_precision, [min, max]>
        high_precision_type_rule = lit("number") >> char_('<') >> lit("high_precision") >> char_(',')
        >> ( char_('(') | char_('[') )  [at_c<3>(_val) = _1]
        >> double_                      [at_c<0>(_val) = _1]
        >> char_(',')
        >> double_                      [at_c<1>(_val) = _1]
        >> ( char_(')') | char_(']') )  [at_c<4>(_val) = _1]
        >> char_(',')
        >> double_                      [at_c<2>(_val) = _1]
        >> char_('>') ;
        
        // nothing, bundle<>, numeric<integer, ...>, numeric<real, ...> or numeric<high_precision, ...>
        type_rule = ( bundle_type_rule | integer_type_rule | real_type_rule | high_precision_type_rule )[_val = _1] ;
        
        // type_name identifier;
        variable_declaration_rule = ( type_rule[at_c<0>(_val) = _1] >> identifier_rule[at_c<1>(_val) = _1] >> char_(';') ) ;
        
        // bundle name { type_name v0; ... type_name vn; };
        bundle_declaration_rule = lit("bundle ") >> identifier_rule[at_c<0>(_val) = _1]
        >> char_('{') >> +( variable_declaration_rule[push_back(at_c<1>(_val), _1)] )
        >> char_('}') >> char_(';') ;
        
        // instance_of<component_type_name> s0;
        sub_declaration_rule = lit("instance_of") >> char_('<') >> identifier_rule[at_c<0>(_val) = _1] >> char_('>')
        >> identifier_rule[at_c<1>(_val) = _1] >> char_(';') ;
        
        // name as type
        parameter_declaration_rule = ( identifier_rule[at_c<0>(_val) = _1] >> char_('=') >> type_rule[at_c<1>(_val) = _1] ) ;
        
        // name as type, ..., name as type
        parameter_list_declaration_rule = ( parameter_declaration_rule[push_back(_val, _1)] >> *( char(',') >> parameter_declaration_rule[push_back(_val, _1)] ) ) ;
        
        // name as type / nothing
        agnostic_parameters_declaration_rule = ( nothing_rule | parameter_list_declaration_rule )[_val = _1] ;
        
        // return nothing / return type
        agnostic_type_rule = ( type_rule | nothing_rule )[_val = _1] ;
        
        // use_case name: return ... | read ... | modify ... ;
        use_case_declaration_rule = lit("use_case ") >> identifier_rule[at_c<0>(_val) = _1] >> char_(':')
        >> lit("return ") >> agnostic_type_rule[at_c<1>(_val) = _1] >> char_('|')
        >> lit("read ")   >> agnostic_parameters_declaration_rule[at_c<2>(_val) = _1] >> char_('|')
        >> lit("modify ") >> agnostic_parameters_declaration_rule[at_c<3>(_val) = _1] >> char_(';') ;
        
        // component name
        // {
        // sub:
        //     instance_of<component_type_name> s0;
        //     ...
        // knowledge:
        //     type_name v0;
        //     ...
        // interface:
        //     use_case name: return ... | read ... | modify ... ;
        //     ...
        // };
        component_declaration_rule = lit("component ") >> identifier_rule[at_c<0>(_val) = _1] >> char_('{')
        >> lit("sub:") >> *( sub_declaration_rule[push_back(at_c<1>(_val), _1)] )
        >> lit("knowledge:") >> *( variable_declaration_rule[push_back(at_c<2>(_val), _1)] )
        >> lit("interface:") >> *( use_case_declaration_rule[push_back(at_c<3>(_val), _1)] )
        >> char_('}') >> char_(';') ;
        
        // Start rule
        start_rule = *( bundle_declaration_rule[push_back(at_c<1>(_val), _1)] | component_declaration_rule[push_back(at_c<2>(_val), _1)] );
    }
}

#endif /* ast_generator_h */
