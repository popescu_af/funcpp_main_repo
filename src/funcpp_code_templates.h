//
//  funcpp_code_templates.h
//  funC++
//
//  Created by Alexandru Popescu on 03/12/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef funcpp_code_templates_h
#define funcpp_code_templates_h

#include "definitions.h"
#include <string>

namespace funcpp
{
    // funcpp templates
    std::string create_funcpp_main_component(const std::string& component_name);
    
    // c++ templates
    std::string create_cpp_class(const std::string& class_name, const uint32_t indent_level,
                                 const string_vector& apis, const string_vector& sub_components, const string_vector& attributes);
    
    // cmake templates
    std::string create_cmakelists_header(const std::string& project_name, const uint32_t project_major_version, const uint32_t project_minor_version);
    std::string create_cmakelists_lib_target(const std::string& target_name, const string_vector& sources);
    std::string create_cmakelists_app_target(const std::string& target_name, const string_vector& sources);
}

#endif /* funcpp_code_templates_h */
