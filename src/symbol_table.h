//
//  symbol_table.h
//  funC++
//
//  Created by Alexandru Popescu on 05/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef symbol_table_h
#define symbol_table_h

#include <set>
#include <string>
#include <unordered_map>
#include <vector>

namespace funcpp
{
    typedef std::vector<std::string> DependencyList;
    
    enum class SymbolType
    {
        e_bundle = 0,
        e_component
    };
    
    struct SymbolTableEntry
    {
        std::string    name;
        std::string    filename;
        std::string    generated_code;
        DependencyList needed_symbols;
        SymbolType     type;
        bool           exported;
        bool           reserved[3];
    };
    
    typedef std::unordered_map<std::string, SymbolTableEntry> SymbolMap;
    typedef std::set<std::string> SetOfStrings;
    
    struct SymbolTable
    {
        SymbolMap    table;
        SetOfStrings expected_symbols;
    };
}

#endif /* symbol_table_h */
