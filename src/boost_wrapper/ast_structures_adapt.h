//
//  ast_structures_adapt.h
//  funC++
//
//  Created by Alexandru Popescu on 23/09/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef ast_structures_adapt_h
#define ast_structures_adapt_h

BOOST_FUSION_ADAPT_STRUCT(
                          funcpp::SourceFileAst,
                          (std::string, name),
                          (funcpp::BundleDeclarations, bundles),
                          (funcpp::ComponentDeclarations, components)
                          )

BOOST_FUSION_ADAPT_STRUCT(
                          funcpp::NothingType,
                          )

BOOST_FUSION_ADAPT_STRUCT(
                          funcpp::BundleType,
                          (std::string, bundle_type_name)
                          )

BOOST_FUSION_ADAPT_STRUCT(
                          funcpp::IntegerType,
                          (int64_t, lower_bound_value),
                          (int64_t, upper_bound_value),
                          (int64_t, init_value),
                          (char, lower_bound),
                          (char, upper_bound)
                          )

BOOST_FUSION_ADAPT_STRUCT(
                          funcpp::RealType,
                          (float, lower_bound_value),
                          (float, upper_bound_value),
                          (float, init_value),
                          (char,  lower_bound),
                          (char,  upper_bound)
                          )

BOOST_FUSION_ADAPT_STRUCT(
                          funcpp::HighPrecisionType,
                          (double, lower_bound_value),
                          (double, upper_bound_value),
                          (double, init_value),
                          (char,   lower_bound),
                          (char,   upper_bound)
                          )

BOOST_FUSION_ADAPT_STRUCT(
                          funcpp::VariableDeclaration,
                          (funcpp::TypeGeneric, type),
                          (std::string, name)
                          )

BOOST_FUSION_ADAPT_STRUCT(
                          funcpp::BundleDeclaration,
                          (std::string, name),
                          (funcpp::VariableDeclarations, variables),
                          )

BOOST_FUSION_ADAPT_STRUCT(
                          funcpp::SubDeclaration,
                          (std::string, type),
                          (std::string, name),
                          )

BOOST_FUSION_ADAPT_STRUCT(
                          funcpp::Parameter,
                          (std::string, name),
                          (funcpp::TypeGeneric, type),
                          )

BOOST_FUSION_ADAPT_STRUCT(
                          funcpp::UseCaseDeclaration,
                          (std::string, name),
                          (funcpp::AgnosticType, return_type),
                          (funcpp::AgnosticParameters, read_parameters),
                          (funcpp::AgnosticParameters, modify_parameters),
                          )

BOOST_FUSION_ADAPT_STRUCT(
                          funcpp::ComponentDeclaration,
                          (std::string, name),
                          (funcpp::SubDeclarations, subs)
                          (funcpp::VariableDeclarations, variables),
                          (funcpp::UseCaseDeclarations, use_cases),
                          )

#endif /* ast_structures_adapt_h */
