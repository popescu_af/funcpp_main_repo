//
//  ast_sub_traverser.h
//  funC++
//
//  Created by Alexandru Popescu on 18/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef ast_sub_traverser_h
#define ast_sub_traverser_h

#include <algorithm>
#include <ast_type_visitor.h>
#include <boost/variant/static_visitor.hpp>
#include <definitions.h>
#include <sstream>
#include <symbol_table.h>

namespace funcpp
{
    struct SubTraverser
    {
        SubTraverser(DependencyList& dependency_collector)
        : m_dependencies(dependency_collector)
        {
        }
        
        void operator() (const SubDeclarations& subs, const bool first_initialization, const std::string& tabs);
        void operator() (const SubDeclaration&  sub , const bool clear_stream = true);
        
        std::stringstream m_ss_decl; // Stream for sub declaration
        std::stringstream m_ss_init; // Stream for initialisation in constructor's initialisation list
        
        DependencyList& m_dependencies;
        
        bool m_all_ok;
        bool m_reserved[7];
    };
    
    inline
    void SubTraverser::operator() (const SubDeclarations& subs, const bool first_initialization, const std::string& tabs)
    {
        m_all_ok = true;
        m_ss_decl.str(std::string()); // clear contents
        m_ss_init.str(std::string());
        
        if ( !subs.empty() )
        {
            m_ss_decl << "\n\t";
            if (first_initialization)
                m_ss_init << "\n" << tabs << ": ";
            else
                m_ss_init << "\n" << tabs << ", ";
                
            (*this)(subs.front(), false);
            if (!m_all_ok)
                return;
            
            for (auto it = subs.begin() + 1, end = subs.end(); it != end; ++it)
            {
                m_ss_decl << "\n\t";
                m_ss_init << "\n" << tabs << ", ";
                (*this)(*it, false);
                if (!m_all_ok)
                    return;
            }
        }
    }
    
    inline
    void SubTraverser::operator() (const SubDeclaration& sub, const bool clear_stream /* = true */)
    {
        m_all_ok = true;
        if (clear_stream)
        {
            m_ss_decl.str(std::string()); // clear contents
            m_ss_init.str(std::string());
        }
        
        m_ss_decl << k_pointer_type << "<" << sub.type << "> " << sub.name << ";";
        m_ss_init << sub.name << "()";
    }
}

#endif /* ast_sub_traverser_h */
