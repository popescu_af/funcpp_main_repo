//
//  translator.h
//  funC++
//
//  Created by Alexandru Popescu on 28/09/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef translator_h
#define translator_h

#include <ast_generator_wrapper.h>
#include <ast_traverser.h>
#include <fs_utils.h>
#include <iostream>
#include <symbol_table.h>

namespace funcpp
{
    struct Translator
    {
        bool operator() (const std::string& filename);
        
    public: // visible attributes
        SourceFileAst m_ast;
        SymbolTable   m_sym_table;
        
    private:
        bool preprocess();
        bool generate_ast();
        bool traverse_ast();
        
    private:
        std::string m_source_file_text;
    };
    
    inline
    bool Translator::operator() (const std::string& filename)
    {
        std::cout << "Translating " << filename << ": ";
        
        return (read_text_file(filename, m_source_file_text)
            &&  preprocess()
            &&  generate_ast()
            &&  traverse_ast()
            );
    }
    
    inline
    bool Translator::preprocess()
    {
        return true;
    }
    
    inline
    bool Translator::generate_ast()
    {
        return generate_ast_static(m_source_file_text, m_ast);
    }
    
    inline
    bool Translator::traverse_ast()
    {
        AstTraverser traverser(m_ast, m_sym_table);
        return traverser();
    }
}

#endif /* translator_h */
