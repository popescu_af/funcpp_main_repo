//
//  ast_traverser.h
//  funC++
//
//  Created by Alexandru Popescu on 04/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef ast_traverser_h
#define ast_traverser_h

#include <algorithm>
#include <ast_sub_traverser.h>
#include <ast_use_case_traverser.h>
#include <ast_variable_traverser.h>
#include <boost/variant/static_visitor.hpp>
#include <sstream>
#include <symbol_table.h>

namespace funcpp
{
    struct AstTraverser
    {
        AstTraverser(const SourceFileAst& ast, SymbolTable& sym_table)
        : m_ast_ref(ast)
        , m_sym_table_ref(sym_table)
        {
        }
        
        bool operator() ();
        bool operator() (const BundleDeclaration& bundle);
        bool operator() (const ComponentDeclaration& component);
        
    private:
        const SourceFileAst& m_ast_ref;
              SymbolTable&   m_sym_table_ref;
    };
    
    inline
    bool AstTraverser::operator() ()
    {
        // Process bundles
        const auto itBundles = std::find_if_not(m_ast_ref.bundles.begin(), m_ast_ref.bundles.end(), *this);
        if (itBundles != m_ast_ref.bundles.end())
            return false;
        
        // Process components
        const auto itComponents = std::find_if_not(m_ast_ref.components.begin(), m_ast_ref.components.end(), *this);
        
        // Exit
        return (itComponents == m_ast_ref.components.end());
    }
    
    /*
     A bundle will be materialized into:
     struct name
     {
        <variable declarations>
     
        name()
            : <initialization list>
        {
        }
     };
     */
    inline
    bool AstTraverser::operator() (const BundleDeclaration& bundle)
    {
        SymbolTableEntry new_entry;
        new_entry.type = SymbolType::e_bundle;
        new_entry.exported = false;
        new_entry.name = bundle.name;
        new_entry.filename = m_ast_ref.name + ".h";
        new_entry.needed_symbols.clear();
        
        std::stringstream ss_struct;
        std::stringstream ss_ctor;
        
        // First part of the struct
        ss_struct << "struct " << new_entry.name << "\n";
        ss_struct << "{";
        ss_ctor << "\t" << new_entry.name << "()";
        
        // Add each variable to the struct and to the constructor's initialization list
        VariableTraverser vt(new_entry.needed_symbols);
        vt(bundle.variables, true, "\t\t");
        if (!vt.m_all_ok)
            return false;
        ss_struct << vt.m_ss_decl.str() << "\n";
        ss_ctor   << vt.m_ss_init.str() << "\n";
        
        // Finish ctor()
        ss_ctor << "\t{\n\t}\n";
        
        // Add ctor stream to struct stream
        ss_struct << "\n" << ss_ctor.str();
        ss_struct << "};\n";
        
        // Commit code into the symbol table entry
        new_entry.generated_code = ss_struct.str();
        
        if ( m_sym_table_ref.table.find(new_entry.name) == m_sym_table_ref.table.end() )
        {
            m_sym_table_ref.table[new_entry.name] = new_entry;
            return true;
        }
        return false;
    }
    
    /*
     A component will be materialized into:
     name.h:
     class name
     {
     public:
        <APIs>
     
     private: // Sub-components
        <sub-components>
     
     private: // Attributes
        <variable declarations>
     };
     
     name.cpp:
     name::name()
        : <initialization list>
     {
     }
     
     <APIs>
     */
    inline
    bool AstTraverser::operator() (const ComponentDeclaration& component)
    {
        SymbolTableEntry new_entry;
        new_entry.type = SymbolType::e_component;
        new_entry.exported = false;
        new_entry.name = component.name;
        new_entry.filename = component.name + ".hpp";
        new_entry.needed_symbols.clear();
        
        std::stringstream ss_header;
        std::stringstream ss_cpp_init;
        std::stringstream ss_cpp_methods;
        
        // First part of the class
        ss_header << "class " << new_entry.name << "\n";
        ss_header << "{\n";
        ss_header << "public:";
        
        // Parse and inline use cases
        if (!component.use_cases.empty())
        {
            UseCaseTraverser uct(new_entry.name, new_entry.needed_symbols);
            uct(component.use_cases);
            if (!uct.m_all_ok)
                return false;
            
            ss_header      << uct.m_ss_header.str() << "\n";
            ss_cpp_methods << uct.m_ss_cpp.str();
        }
        
        ss_header << "\n";
        ss_header << "private: // Sub-Components";
        
        // Ctor code
        ss_cpp_init << new_entry.name << "::" << new_entry.name << "()";
        
        // Parse and inline sub-components
        if (!component.subs.empty())
        {
            SubTraverser st(new_entry.needed_symbols);
            st(component.subs, true, "\t");
            if (!st.m_all_ok)
                return false;
            
            ss_header   << st.m_ss_decl.str() << "\n";
            ss_cpp_init << st.m_ss_init.str();
        }
        
        ss_header << "\n";
        ss_header << "private: // Attributes";
        
        // Parse and inline attributes
        if (!component.variables.empty())
        {
            VariableTraverser vt(new_entry.needed_symbols);
            vt(component.variables, component.subs.empty(), "\t");
            if (!vt.m_all_ok)
                return false;
            
            ss_header   << vt.m_ss_decl.str() << "\n";
            ss_cpp_init << vt.m_ss_init.str();
        }
        
        ss_header << "};\n";
        
        // Finish ctor()
        ss_cpp_init << "\n{\n}\n";
        
        std::stringstream ss_final;
        ss_final << ss_header.str() << k_header_delimiter << "\n" << ss_cpp_init.str() << ss_cpp_methods.str();
        
        // Commit code into the symbol table entry
        new_entry.generated_code = ss_final.str();
        
        if ( m_sym_table_ref.table.find(new_entry.name) == m_sym_table_ref.table.end() )
        {
            m_sym_table_ref.table[new_entry.name] = new_entry;
            return true;
        }
        return false;
    }
}

#endif /* ast_traverser_h */
