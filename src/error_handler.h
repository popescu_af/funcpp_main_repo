//
//  error_handler.h
//  funC++
//
//  Created by Alexandru Popescu on 15/11/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef error_handler_h
#define error_handler_h

#include <definitions.h>
#include <iostream>

namespace funcpp
{
    enum ErrorCode // MUST be non-negative
    {
        success = 0,
        undefined    // The limit
    };
    
    typedef std::vector<ErrorCode> ErrorCodes;
    
    inline
    ErrorCode& error_code()
    {
        static ErrorCode err_code = ErrorCode::success;
        return err_code;
    }
    
    inline
    ErrorCodes& mt_error_codes()
    {
        static ErrorCodes err_codes = ErrorCodes(k_num_threads, ErrorCode::success);
        return err_codes;
    }
    
    inline
    const string_vector& error_descriptions()
    {
        static string_vector descriptions = {
            "Success."
        };
        return descriptions;
    }
    
    inline
    void print_error_status()
    {
        if (error_code() == success)
        {
            std::cout << "Operation completed successfully";
            return;
        }
        const size_t val_err_code = static_cast<size_t>(error_code());
        std::cerr << "Error(" << val_err_code << "): " << error_descriptions()[val_err_code];
    }
}

#define FUNCPP_FAIL(err_code) return (error_code() = err_code)
#define FUNCPP_EXIT() FUNCPP_FAIL(success)

#endif /* error_handler_h */
