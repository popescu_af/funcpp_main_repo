//
//  ast_variable_traverser.h
//  funC++
//
//  Created by Alexandru Popescu on 12/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef ast_variable_traverser_h
#define ast_variable_traverser_h

#include <algorithm>
#include <ast_type_visitor.h>
#include <boost/variant/static_visitor.hpp>
#include <sstream>
#include <symbol_table.h>

namespace funcpp
{
    struct VariableTraverser
    {
        VariableTraverser(DependencyList& dependency_collector)
        : m_type_visitor(dependency_collector)
        {
        }
        
        void operator() (const VariableDeclarations& variables, const bool first_initialization, const std::string& tabs);
        void operator() (const VariableDeclaration&  variable , const bool clear_stream = true);
        
        std::stringstream m_ss_decl; // Stream for type declaration
        std::stringstream m_ss_init; // Stream for initialisation in constructor's initialisation list
        
        bool m_all_ok;
        bool m_reserved[7];
        
    private:
        TypeVisitor m_type_visitor;
    };
    
    inline
    void VariableTraverser::operator() (const VariableDeclarations& variables, const bool first_initialization, const std::string& tabs)
    {
        m_all_ok = true;
        m_ss_decl.str(std::string()); // clear contents
        m_ss_init.str(std::string());
        
        if ( !variables.empty() )
        {
            m_ss_decl << "\n\t";
            if (first_initialization)
                m_ss_init << "\n" << tabs << ": ";
            else
                m_ss_init << "\n" << tabs << ", ";
            (*this)(variables.front(), false);
            if (!m_all_ok)
                return;
            
            for (auto it = variables.begin() + 1, end = variables.end(); it != end; ++it)
            {
                m_ss_decl << "\n\t";
                m_ss_init << "\n" << tabs << ", ";
                (*this)(*it, false);
                if (!m_all_ok)
                    return;
            }
        }
    }
    
    inline
    void VariableTraverser::operator() (const VariableDeclaration& variable, const bool clear_stream /* = true */)
    {
        m_all_ok = true;
        if (clear_stream)
        {
            m_ss_decl.str(std::string()); // clear contents
            m_ss_init.str(std::string());
        }
        
        boost::apply_visitor(m_type_visitor, variable.type); // puts the appropriate type on the stream
        if (!m_type_visitor.m_all_ok)
        {
            m_all_ok = false;
            return;
        }
        
        m_ss_decl << m_type_visitor.m_ss_decl.str() << " " << variable.name << ";";
        m_ss_init << variable.name << m_type_visitor.m_ss_ctor.str();
    }
}

#endif /* ast_variable_traverser_h */
