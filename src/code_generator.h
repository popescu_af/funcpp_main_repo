//
//  code_generator.h
//  funC++
//
//  Created by Alexandru Popescu on 05/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef code_generator_h
#define code_generator_h

#include <symbol_table.h>

namespace funcpp
{
    struct CodeGenerator
    {
        void operator() (SymbolTable& sym_table);
    };
}

#endif /* code_generator_h */
