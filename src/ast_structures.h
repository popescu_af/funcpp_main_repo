//
//  ast_structures.h
//  funC++
//
//  Created by Alexandru Popescu on 23/09/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef ast_structures_h
#define ast_structures_h

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/variant/variant.hpp>

#include <commons.h>
#include <string>
#include <vector>

namespace funcpp
{
    struct BundleDeclaration;
    typedef std::vector<BundleDeclaration> BundleDeclarations;
    
    struct ComponentDeclaration;
    typedef std::vector<ComponentDeclaration> ComponentDeclarations;
    
    struct SourceFileAst
    {
        std::string name;
        BundleDeclarations bundles;
        ComponentDeclarations components;
    };
    
    struct SubDeclaration;
    typedef std::vector<SubDeclaration> SubDeclarations;
    
    struct VariableDeclaration;
    typedef std::vector<VariableDeclaration> VariableDeclarations;
    
    struct UseCaseDeclaration;
    typedef std::vector<UseCaseDeclaration> UseCaseDeclarations;
    
    struct BundleDeclaration
    {
        std::string name;
        VariableDeclarations variables;
    };
    
    struct ComponentDeclaration
    {
        std::string name;
        SubDeclarations subs;
        VariableDeclarations variables;
        UseCaseDeclarations use_cases;
    };
    
    struct SubDeclaration
    {
        std::string type;
        std::string name;
    };
    
    struct NothingType
    {
    };
    
    struct BundleType
    {
        std::string bundle_type_name;
    };
    
    struct IntegerType
    {
        int64_t lower_bound_value;
        int64_t upper_bound_value;
        int64_t init_value;
        char    lower_bound;
        char    upper_bound;
        char    reserved[6];
        
        IntegerType()
        {
            UNUSED(reserved[0]);
        }
    };
    
    struct RealType
    {
        float lower_bound_value;
        float upper_bound_value;
        float init_value;
        char  lower_bound;
        char  upper_bound;
        char  reserved[2];
        
        RealType()
        {
            UNUSED(reserved[0]);
        }
    };
    
    struct HighPrecisionType
    {
        double lower_bound_value;
        double upper_bound_value;
        double init_value;
        char   lower_bound;
        char   upper_bound;
        char   reserved[6];
        
        HighPrecisionType()
        {
            UNUSED(reserved[0]);
        }
    };
    
    typedef boost::variant<BundleType, IntegerType, RealType, HighPrecisionType> TypeGeneric;
    
    struct VariableDeclaration
    {
        TypeGeneric type;
        std::string name;
    };
    
    typedef boost::variant<NothingType, TypeGeneric> AgnosticType;
    
    struct Parameter;
    typedef std::vector<Parameter> Parameters;
    typedef boost::variant<NothingType, Parameters> AgnosticParameters;
    
    struct UseCaseDeclaration
    {
        std::string name;
        AgnosticType return_type;
        AgnosticParameters read_parameters;
        AgnosticParameters modify_parameters;
    };
    
    struct Parameter
    {
        std::string name;
        TypeGeneric type;
    };
}

// Adaptations next
#include <boost_wrapper/ast_structures_adapt.h>

#endif /* ast_structures_h */
