//
//  project_handler.h
//  funC++
//
//  Created by Alexandru Popescu on 01/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef project_handler_h
#define project_handler_h

#include <algorithm>
#include <commons.h>
#include <definitions.h>
#include <fs_utils.h>
#include <fstream>
#include "funcpp_code_templates.h"
#include <iostream>
#include <memory>
#include <string>
#include <string_utils.h>
#include "translator.h"

namespace funcpp
{
    struct ProjectHandler;
    typedef std::unique_ptr<ProjectHandler> ProjectPtr;
    
    struct ProjectHandler
    {
        ~ProjectHandler();
        
        static bool create(const std::string& name, const std::string& path, const std::string& type);
        static ProjectPtr open(const std::string& path);
        
        bool add(const std::string& descriptor_path);
        bool add_new(const std::string& descriptor_name);
        bool generate_cpp();
        bool close();
        
    public: // visible attributes
        std::string   m_path;
        std::string   m_product_name;
        std::string   m_project_type;
        string_vector m_exported;
        string_vector m_descriptors;
        
    private:
        ProjectHandler();
        
    private: // definitions
        enum class ReadState
        {
            e_reading_product_name = 0,
            e_reading_product_type,
            e_reading_export,
            e_reading_descr,
            e_inconsistent
        };
        
    private:
        Translator m_translator;
        ReadState  m_state;
        int32_t    m_reserved;
    };
    
    inline
    ProjectHandler::ProjectHandler()
    : m_state(ReadState::e_inconsistent)
    {
        UNUSED(m_reserved);
    }
    
    inline
    ProjectHandler::~ProjectHandler()
    {
        close();
    }
    
    inline
    bool ProjectHandler::create(const std::string& name, const std::string& path, const std::string& type)
    {
        const std::string prj_file = path + "/" + k_project_file_name;
        if ( file_dir_exists(prj_file) )
        {
            std::cerr << "Error: a funcpp project is already set up under the specified path.\n\n";
            return false;
        }
        
        const bool directories_ok =
            create_dir(path + "/desc") &&
            create_dir(path + "/c++" ) ;
        
        if (!directories_ok)
        {
            std::cerr << "Error: could not create project directories.\n\n";
            return false;
        }
        
        // Add main descriptor
        const std::string main_desc_file = path + "/desc/" + name + k_desc_file_ext;
        if (file_dir_exists(main_desc_file))
        {
            std::cerr << "Error: " << main_desc_file << " already exists! Could not create the main descriptor file.\n\n";
            return false;
        }
        
        std::ofstream out(main_desc_file, std::ios_base::out);
        if (!out)
        {
            std::cerr << "Error: could not create the main descriptor file.\n\n";
            return false;
        }
        
        out << create_funcpp_main_component(name);
        out.close();
        
        out = std::ofstream(prj_file, std::ios_base::out);
        if (!out)
        {
            std::cerr << "Error: could not create the project file.\n\n";
            return false;
        }
        
        out << "PRODUCT:\n" << name << "\n\nTYPE:\n" << type << "\n\nEXPORT:\n\nDESCRIPTOR:\n" << name << "\n\n";
        out.close();
        
        return true;
    }
    
    inline
    ProjectPtr ProjectHandler::open(const std::string& path)
    {
        ProjectPtr result;
        
        // See standard.txt.
        const auto filename = path + "/" + k_project_file_name;
        if ( !file_dir_exists(filename) )
        {
            std::cerr << "Error: invalid path or no funcpp project under the path specified.\n\n";
            return nullptr;
        }
        
        std::string text;
        if ( !read_text_file(filename, text) )
        {
            std::cerr << "Error: could not read project file.\n\n";
            return nullptr;
        }
        
        // Init
        result.reset( new ProjectHandler() );
        result->m_path  = path;
        result->m_state = ReadState::e_inconsistent;
        result->m_descriptors.clear();
        
        auto lines = split_into_lines(text);
        for (auto& line : lines)
        {
            line = trim(line);
            if (line == "")
                continue;
            
            // Test to see if a tag has been reached
            {
                const auto found_it = std::find(k_known_funcpp_tags.begin(), k_known_funcpp_tags.end(), line);
                if ( found_it != k_known_funcpp_tags.end() )
                {
                    result->m_state = static_cast<ReadState>( std::distance(k_known_funcpp_tags.begin(), found_it) );
                    continue;
                }
            }
            
            switch (result->m_state)
            {
                case ReadState::e_reading_product_name:
                    result->m_product_name = line;
                    break;
                    
                case ReadState::e_reading_product_type:
                {
                    result->m_project_type = line;
                    const auto found_it = std::find(k_known_funcpp_prj_types.begin(), k_known_funcpp_prj_types.end(), result->m_project_type);
                    if (found_it == k_known_funcpp_prj_types.end())
                    {
                        std::cerr << "Error: unknown project type.\n\n";
                        return nullptr;
                    }
                    break;
                }
                    
                case ReadState::e_reading_export:
                    result->m_exported.push_back(line);
                    break;
                    
                case ReadState::e_reading_descr:
                    result->m_descriptors.push_back(line);
                    break;
                    
                case ReadState::e_inconsistent:
                    std::cerr << "Error: unknown tag at line 0.\n\n";
                    return nullptr;
            }
        }
        return result;
    }
    
    inline
    bool ProjectHandler::add(const std::string& descriptor_path)
    {
        auto pos_short_name = descriptor_path.rfind('/');
        if (pos_short_name == std::string::npos)
            pos_short_name = 0;
        
        const auto len_short_name = descriptor_path.rfind('.') - 1 - pos_short_name;
        const auto short_name = descriptor_path.substr(pos_short_name + 1, len_short_name);
        
        const auto found_it = std::find(m_descriptors.begin(), m_descriptors.end(), short_name);
        if (found_it != m_descriptors.end())
        {
            std::cerr << "Error: project already contains a descriptor with this name.\n\n";
            return false;
        }
        
        m_descriptors.push_back(short_name);
        std::stable_sort(m_descriptors.begin(), m_descriptors.end());
        
        const std::string new_desc_name = m_path + "/desc/" + short_name + k_desc_file_ext;
        copy_file(descriptor_path, new_desc_name);
        return true;
    }
    
    inline
    bool ProjectHandler::add_new(const std::string& descriptor_name)
    {
        const auto found_it = std::find(m_descriptors.begin(), m_descriptors.end(), descriptor_name);
        if (found_it != m_descriptors.end())
        {
            std::cerr << "Error: project already contains a descriptor with this name.\n\n";
            return false;
        }
        
        m_descriptors.push_back(descriptor_name);
        std::stable_sort(m_descriptors.begin(), m_descriptors.end());
        
        const std::string new_desc_name = m_path + "/desc/" + descriptor_name + k_desc_file_ext;
        return create_empty_file(new_desc_name);
    }
    
    inline
    bool ProjectHandler::generate_cpp()
    {
        string_vector cmake_sources;
        SymbolTable global_sym_table;
        
        // Get symbols from project
        for (const auto& desc_name : m_descriptors)
        {
            m_translator(m_path + "/desc/" + desc_name + k_desc_file_ext);
            
            for (const auto& sym : m_translator.m_sym_table.table)
            {
                if ( global_sym_table.table.find(sym.first) != global_sym_table.table.end() )
                {
                    std::cerr << "Error: duplicate symbol.\n\n";
                    return false;
                }
                
                global_sym_table.table[sym.first] = sym.second;
            }
            
            global_sym_table.expected_symbols.insert(m_translator.m_sym_table.expected_symbols.begin(), m_translator.m_sym_table.expected_symbols.end());
        }
        
        // Get symbols from project's dependencies
        // TODO
        
        // Check dependencies
        for (const auto& dependency : global_sym_table.expected_symbols)
        {
            if ( global_sym_table.table.find(dependency) == global_sym_table.table.end() )
            {
                std::cerr << "Error: undefined symbol " << dependency << ".\n\n";
                return false;
            }
        }
        
        // Proceed to writing
        
        return true;
    }
    
    inline
    bool ProjectHandler::close()
    {
        std::ofstream out(m_path + "/" + k_project_file_name, std::ios_base::out);
        if (!out)
            return false;
        
        out << "PRODUCT:\n" << m_product_name << "\n\nTYPE:\n" << m_project_type << "\n\nEXPORT:\n\nDESCRIPTOR:\n";
        for (const auto& desc : m_descriptors)
            out << desc << "\n";
        
        out.close();
        return true;
    }
}

#endif /* project_handler_h */
