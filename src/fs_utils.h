//
//  fs_utils.h
//  funC++
//
//  Created by Alexandru Popescu on 01/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef fs_utils_h
#define fs_utils_h

#include <boost/filesystem.hpp>
#include <fstream>
#include <iostream>
#include <string>

namespace funcpp
{
    namespace bfs = boost::filesystem;
    
    inline
    bool read_text_file(const std::string& filename, std::string& result)
    {
        std::ifstream in(filename, std::ios_base::in);
        if (!in)
        {
            std::cerr << "Error: Could not open input file: " << filename << "." << std::endl;
            return false;
        }
        in.unsetf(std::ios::skipws); // No white space skipping!
        
        result.clear();
        std::copy(std::istream_iterator<char>(in), std::istream_iterator<char>(), std::back_inserter(result));
        return true;
    }
    
    inline
    bool file_dir_exists(const std::string& filename)
    {
        return bfs::exists(filename);
    }
    
    inline
    bool create_dir(const std::string& path)
    {
        return bfs::create_directory(path);
    }
    
    inline
    bool remove_file_dir(const std::string& path)
    {
        return bfs::remove_all(path);
    }
    
    inline
    void copy_file(const std::string& src, const std::string& dst)
    {
        if (src == dst)
            return;
        
        bfs::copy_file(src, dst);
    }
    
    inline
    bool create_empty_file(const std::string& filename)
    {
        std::ofstream out(filename, std::ios_base::out);
        if (!out)
        {
            std::cerr << "Error: Could not create file: " << filename << ".\n\n";
            return false;
        }
        out.close();
        return true;
    }
}

#endif /* fs_utils_h */
