//
//  string_utils.h
//  funC++
//
//  Created by Alexandru Popescu on 01/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef string_utils_h
#define string_utils_h

#include <algorithm>
#include <definitions.h>
#include <sstream>
#include <string>
#include <vector>

namespace funcpp
{
    inline
    string_vector split_into_lines(const std::string& text)
    {
        string_vector result;
        std::stringstream ss(text);
        std::string line;
        
        while ( std::getline(ss, line) )
            result.push_back(line);
        
        return result;
    }
    
    inline
    std::string trim(const std::string& text)
    {
        const auto first_non_white = std::find_if_not(text.begin(), text.end(), isspace);
        if (first_non_white == text.end())
            return "";
        
        const auto trim_start = std::distance( text.begin (), first_non_white );
        const auto trim_end   = std::distance( text.rbegin(), std::find_if_not(text.rbegin(), text.rend(), isspace) );
        
        const auto to_copy = static_cast<long>(text.length()) - (trim_start + trim_end);
        if (to_copy > 0)
            return text.substr( static_cast<size_t>(trim_start), static_cast<size_t>(to_copy) );
        
        return "";
    }
}

#endif /* string_utils_h */
