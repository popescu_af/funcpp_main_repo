//
//  ast_type_visitor.h
//  funC++
//
//  Created by Alexandru Popescu on 13/10/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#ifndef ast_type_visitor_h
#define ast_type_visitor_h

#include <algorithm>
#include <ast_structures.h>
#include <boost/variant/static_visitor.hpp>
#include <sstream>
#include <symbol_table.h>

namespace funcpp
{
    typedef std::vector<std::string> TypesList;
    
    template<typename NumericType>
    bool numericOk(const NumericType& type)
    {
        const bool wrong_bounds   = (type.lower_bound_value >= type.upper_bound_value);
        const bool init_outb_lt   = (type.lower_bound == '[' && type.init_value <  type.lower_bound_value);
        const bool init_outb_lteq = (type.lower_bound == '(' && type.init_value <= type.lower_bound_value);
        const bool init_outb_gt   = (type.upper_bound == ']' && type.init_value >  type.upper_bound_value);
        const bool init_outb_gteq = (type.upper_bound == ')' && type.init_value >= type.upper_bound_value);
        return !(wrong_bounds || init_outb_lt || init_outb_lteq || init_outb_gt || init_outb_gteq);
    }
    
    struct TypeVisitor : boost::static_visitor<>
    {
        TypeVisitor(DependencyList& dependency_collector)
        : m_dependencies(dependency_collector)
        {
        }
        
        void operator() (const BundleType& decl);
        void operator() (const IntegerType& type);
        void operator() (const RealType& type);
        void operator() (const HighPrecisionType& type);
        
        std::stringstream m_ss_decl; // Stream for type declaration
        std::stringstream m_ss_ctor; // Stream for initialisation in constructor's initialisation list
    
        DependencyList& m_dependencies;
    
        bool m_all_ok;
        bool m_reserved[7];
    };
    
    inline
    void TypeVisitor::operator() (const BundleType& decl)
    {
        m_all_ok = true;
        m_ss_decl.str(std::string()); // clear contents
        m_ss_ctor.str(std::string());
        
        m_ss_decl << "bundle" << "<" << decl.bundle_type_name << ">";
        m_ss_ctor << "()";
        
        m_dependencies.push_back(decl.bundle_type_name);
    }
    
    inline
    void TypeVisitor::operator() (const IntegerType& type)
    {
        m_all_ok = true;
        m_ss_decl.str(std::string()); // clear contents
        m_ss_ctor.str(std::string());
        
        if ( !numericOk(type) )
        {
            m_all_ok = false;
            return;
        }
        
        bool natural = false;
        if (type.lower_bound_value >= 0)
            natural = true;
        
        if (natural)
        {
            static TypesList unsigned_integer_types = { "uint8_t", "uint16_t", "uint32_t", "uint64_t" };
            static std::vector<uint64_t> max_values = { std::numeric_limits<uint8_t >::max(),
                std::numeric_limits<uint16_t>::max(),
                std::numeric_limits<uint32_t>::max(),
                std::numeric_limits<uint64_t>::max()
            };
            
            const auto it = std::upper_bound(max_values.begin(), max_values.end(), type.upper_bound_value);
            if (it == max_values.end())
            {
                m_all_ok = false;
                return;
            }
            m_ss_decl << unsigned_integer_types[static_cast<size_t>(std::distance(max_values.begin(), it))];
        }
        else
        {
            static TypesList integer_types = { "int8_t", "int16_t", "int32_t", "int64_t" };
            static std::vector<int64_t>  min_values = { std::numeric_limits<int8_t >::min(),
                std::numeric_limits<int16_t>::min(),
                std::numeric_limits<int32_t>::min(),
                std::numeric_limits<int64_t>::min()
            };
            static std::vector<uint64_t> max_values = { std::numeric_limits<int8_t >::max(),
                std::numeric_limits<int16_t>::max(),
                std::numeric_limits<int32_t>::max(),
                std::numeric_limits<int64_t>::max()
            };
            
            const auto it_min = std::lower_bound(min_values.begin(), min_values.end(), type.lower_bound_value);
            if (it_min == min_values.end())
            {
                m_all_ok = false;
                return;
            }
            
            const auto it_max = std::upper_bound(max_values.begin(), max_values.end(), type.upper_bound_value);
            if (it_max == max_values.end())
            {
                m_all_ok = false;
                return;
            }
            
            const auto index = std::max( std::distance(min_values.begin(), it_min), std::distance(max_values.begin(), it_max) );
            m_ss_decl << integer_types[static_cast<size_t>(index)];
        }
        m_ss_ctor << "(" << type.init_value << ")";
    }
    
    inline
    void TypeVisitor::operator() (const RealType& type)
    {
        m_all_ok = true;
        m_ss_decl.str(std::string()); // clear contents
        m_ss_ctor.str(std::string());
        
        if ( !numericOk(type) )
        {
            m_all_ok = false;
            return;
        }
        
        m_ss_decl << "float";
        m_ss_ctor << "(" << type.init_value << ")";
    }
    
    inline
    void TypeVisitor::operator() (const HighPrecisionType& type)
    {
        m_all_ok = true;
        m_ss_decl.str(std::string()); // clear contents
        m_ss_ctor.str(std::string());
        
        if ( !numericOk(type) )
        {
            m_all_ok = false;
            return;
        }
        
        m_ss_decl << "double";
        m_ss_ctor << "(" << type.init_value << ")";
    }
}

#endif /* ast_type_visitor_h */
