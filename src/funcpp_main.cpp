//
//  funcpp_main.cpp
//  funC++
//
//  Created by Alexandru Popescu on 30/09/15.
//  Copyright © 2015 Alexandru Florinel Popescu. All rights reserved.
//

#include <algorithm>
#include <definitions.h>
#include <error_handler.h>
#include <funcpp_main.h>
#include <project_handler.h>

using namespace funcpp;

namespace
{
    // Help printers
    typedef std::pair<std::string, std::string> command_help;
    typedef std::vector<command_help> cmd_help_vector;
    
    const cmd_help_vector k_known_helpers = {
        { "help"    , k_usage_text      },
        { "create"  , k_create_cmd_text },
        { "add"     , k_add_cmd_text    },
        { "new"     , k_new_cmd_text    },
        { "c++"     , k_cpp_cmd_text    }
    };
    
    // Handlers
    typedef int (*cmd_handler)(int argc, char **argv);
    typedef std::pair<std::string, cmd_handler> command_detail;
    typedef std::vector<command_detail> command_vector;
    
    inline int handler_help  (int argc, char **argv);
    inline int handler_create(int argc, char **argv);
    inline int handler_add   (int argc, char **argv);
    inline int handler_new   (int argc, char **argv);
    inline int handler_cpp   (int argc, char **argv);
    
    const command_vector k_known_handlers = {
        { "help"  , handler_help   },
        { "create", handler_create },
        { "add"   , handler_add    },
        { "new"   , handler_new    },
        { "c++"   , handler_cpp    }
    };
}

namespace
{
    struct CmdPtrFinder
    {
        CmdPtrFinder(const std::string& str)
        : m_command(str)
        { }
        
        bool operator() (const command_help& helper) const
        {
            return (helper.first == m_command);
        }
        
        bool operator() (const command_detail& detail) const
        {
            return (detail.first == m_command);
        }
        
        std::string m_command;
    };
}

namespace
{
    inline
    int handler_help(int argc, char **argv)
    {
        if (argc == 2)
        {
            std::cout << k_usage_text;
        }
        else if (argc == 3)
        {
            CmdPtrFinder finder(argv[2]);
            const auto foundIt = std::find_if(k_known_helpers.begin(), k_known_helpers.end(), finder);
            if ( foundIt != k_known_helpers.end() )
            {
                std::cout << foundIt->second;
                return 0;
            }
            std::cerr << "Unknown command: " << argv[2] << ".\n\n";
            return 1;
        }
        else
        {
            std::cerr << "Too many parameters. " << k_help_cmd_text;
            return 1;
        }
        return 0;
    }
    
    inline
    int handler_create(int argc, char **argv)
    {
        if (argc < 3)
        {
            std::cerr << "No project name provided. " << k_create_cmd_text;
            return 1;
        }
        
        if (argc > 5)
        {
            std::cerr << "Too many parameters to create. " << k_create_cmd_text;
            return 1;
        }
        
        const std::string prj_name = argv[2];
        std::string prj_type = "lib";
        std::string prj_path = ".";
        
        const std::string setting0( (argc >= 4) ? argv[3] : "" );
        const std::string setting1( (argc == 5) ? argv[4] : "" );
        
        static const std::string k_template = "template=";
        static const std::string k_path     = "path=";
        
        const bool name_is_invalid = (prj_name.find(k_path) == 0 || prj_name.find(k_template) == 0);
        if (name_is_invalid)
        {
            std::cerr << "No project name provided. " << k_create_cmd_text;
            return 1;
        }
        
        const bool setting0_is_template = (setting0.find(k_template) == 0);
        const bool setting1_is_template = (setting1.find(k_template) == 0);
        const bool setting0_is_path     = (setting0.find(k_path    ) == 0);
        const bool setting1_is_path     = (setting1.find(k_path    ) == 0);
        
        if (!setting0.empty() && !setting0_is_template && !setting0_is_path)
        {
            std::cerr << "Invalid setting " << setting0 << ".\n\n";
            return 1;
        }
        
        if (!setting1.empty() && !setting1_is_template && !setting1_is_path)
        {
            std::cerr << "Invalid setting " << setting1 << ".\n\n";
            return 1;
        }
        
        if (setting0_is_template && setting1_is_template)
        {
            std::cerr << "Cannot specify template twice!\n\n";
            return 1;
        }
        
        if (setting0_is_path && setting1_is_path)
        {
            std::cerr << "Cannot specify path twice!\n\n";
            return 1;
        }
        
        if (setting0_is_template)
            prj_type = setting0.substr(k_template.length());
        
        if (setting0_is_path)
            prj_path = setting0.substr(k_path.length());
        
        if (setting1_is_template)
            prj_type = setting1.substr(k_template.length());
        
        if (setting1_is_path)
            prj_path = setting1.substr(k_path.length());
        
        if ( !file_dir_exists(prj_path) )
        {
            std::cerr << "Error: invalid path specified.\n\n";
            return 1;
        }
        
        if ( std::find(k_known_funcpp_prj_types.begin(), k_known_funcpp_prj_types.end(), prj_type) == k_known_funcpp_prj_types.end() )
        {
            std::cerr << "Error: invalid project type - '" << prj_type << "'.\n\n";
            return 1;
        }
        
        if ( !ProjectHandler::create(prj_name, prj_path, prj_type) )
        {
            std::cerr << "Cannot create '" << prj_type << "' project under '" << prj_path << "'.\n\n";
            return 1;
        }
        return 0;
    }
    
    inline
    int handler_add(int argc, char **argv)
    {
        if (argc != 4)
        {
            std::cerr << "Please provide a descriptor path and a project path. " << k_add_cmd_text;
            return 1;
        }
        
        auto prj = ProjectHandler::open(argv[3]);
        if (!prj)
            return 1;
        
        if ( !prj->add(argv[2]) )
            return 1;
        
        return 0;
    }
    
    inline
    int handler_new(int argc, char **argv)
    {
        if (argc != 4)
        {
            std::cerr << "Please provide a descriptor name and a project path. " << k_add_cmd_text;
            return 1;
        }
        
        auto prj = ProjectHandler::open(argv[3]);
        if (!prj)
            return 1;
        
        if ( !prj->add_new(argv[2]) )
            return 1;
        
        return 0;
    }
    
    inline
    int handler_cpp(int argc, char **argv)
    {
        if (argc > 3)
        {
            std::cerr << "Too many parameters to c++. " << k_cpp_cmd_text;
            return 1;
        }
        
        std::string path = ".";
        if (argc == 3)
            path = argv[2];
        
        auto prj = ProjectHandler::open(path);
        if (!prj)
            return 1;
        
        if ( !prj->generate_cpp() )
            return 1;
        
        return 0;
    }
}

namespace funcpp
{
    int main(int argc, char **argv)
    {
        if (argc > 1)
        {
            CmdPtrFinder finder(argv[1]); // argv[1] = 'command' arg
            const auto foundIt = std::find_if(k_known_handlers.begin(), k_known_handlers.end(), finder);
            if ( foundIt != k_known_handlers.end() )
                return foundIt->second(argc, argv);
            
            std::cerr << "Unknown command: " << argv[1] << ".\n\n";
            return 1;
        }
        
        std::cout << k_usage_text;
        return 0;
    }
}
